struct LinkedList {
    cabeza: Box<Nodo>
}

impl LinkedList {
    pub fn new(primer_valor: String) -> LinkedList {
        return LinkedList {
            cabeza: Box::new( Nodo::new( primer_valor ) )
        };
    }

    pub fn agregar_al_principio(&mut self, valor: String) {
        let cabeza_anterior = std::mem::replace( &mut self.cabeza, Box::new( Nodo::new(valor) ) );
        self.cabeza.agregar_siguiente(cabeza_anterior);
    }

    pub fn agregar_al_final(&mut self, valor: String) {

        unsafe {
            let mut ptr_node = &mut self.cabeza as *mut Box<Nodo>;

            while (*ptr_node).siguiente.is_some() {
                let ref_siguiente = &mut (*ptr_node).siguiente;

                ptr_node = ref_siguiente.as_mut().unwrap();
            }

            (*ptr_node).agregar_siguiente(Box::new( Nodo::new(valor) ) );
        };
    }

    pub fn for_each<F>(&self, funcion: F)
        where F: Fn(&Nodo)
    {
        unsafe {
            let mut ptr_nodo = Option::Some( &self.cabeza as *const Box<Nodo> );

            while ptr_nodo.is_some() {

                let nodo_actual = ptr_nodo.unwrap();

                funcion(&*nodo_actual);

                ptr_nodo = match (*ptr_nodo.unwrap()).siguiente.as_ref() {
                    Option::Some(nodo) => Option::Some( nodo as *const Box<Nodo> ),
                    Option::None       => None
                };

            }
        }
    }
}

struct Nodo {
    pub valor    : String,
    pub siguiente: Option< Box<Nodo> >
}

impl Nodo {
    pub fn new(texto: String) -> Nodo {
        return Nodo {
            valor    : texto,
            siguiente: Option::None
        };
    }

    pub fn agregar_siguiente(&mut self, nodo: Box<Nodo>) {
        self.siguiente = Option::Some(nodo);
    }
}

fn main() {
    let mut linkedlist = LinkedList::new( "Soy la cabeza".to_owned() );

    linkedlist.agregar_al_final( "Soy el final".to_owned() );
    linkedlist.agregar_al_principio( "NO! Yo soy la verdadera cabeza".to_owned() );

    linkedlist.for_each( |nodo| { println!("{}", nodo.valor); } );
}
